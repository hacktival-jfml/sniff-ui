#!/usr/bin/env bash

my_pid="$(sudo fuser 80/tcp | grep -o '[^\:]*')"
echo "server found with pid $my_pid"
if [[ -z $my_pid ]]
then
    echo "server not running"
else
    echo "killing server"
    sudo kill $my_pid
fi
