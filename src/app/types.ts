export interface IDeviceDataWithPings extends IDeviceData {
  status: IPing[];
}

export interface IDeviceData {
  deviceId: number;
  ip: string;
  name?: string;
  status?: IPing[];
}

export interface IPing {
  date: Date;
  success: boolean;
}

export interface IEndpointData {
  endpointId: number;
  url: string;
  name: string;
  method: string;
  status?: IEndpointPing[];
}

export interface IEndpointWithPings extends IEndpointData {
  status: IEndpointPing[];
}

export interface IEndpointPing {
  statusMessage: string;
  statusCode: number;
  success: boolean;
  date: Date;
}

export interface IUpTimeBlock {
  successPings: number;
  totalPings: number;
  from: Date;
  to: Date;
  pings: Array<IPing | IEndpointPing>;
}

export interface IUpTimeBlocksInformation {
  upTimeBlocks?: IUpTimeBlock[];
}

export interface IPingTime {
  time: number;
}
