import {IDeviceDataWithPings, IEndpointWithPings, IPing, IUpTimeBlock} from './types';
import * as moment from 'moment';

export const renderTime = (date: Date) => date.toLocaleTimeString([], {hour: '2-digit', minute: '2-digit'});
export const renderDate = (date: Date) => date.toLocaleDateString() + ', ' + renderTime(date);

export const getUpTime = (data: IDeviceDataWithPings | IEndpointWithPings, amountOfIntervals = 6 * 24, intervalLength = 10) => {
  return '.'
    .repeat(amountOfIntervals)
    .split('')
    .map((_, i) => i)
    .reverse()
    .map(b => blockToPingData(data.status, b, intervalLength))
    .reduceRight(
      ([success, nodata, total], a) =>
          [success + a.successPings, a.totalPings === 0 ? nodata + 1 : nodata, total + Math.max(a.totalPings, 1)],
      [0, 0, 0]
    );
};

export const getTimeSlotDates = (timeSlotNo, granularity) => {
  const timeSlotStart = moment(new Date()).subtract({minute: (timeSlotNo + 1) * granularity});
  const timeSlotEnd = moment(new Date()).subtract({minute: (timeSlotNo) * granularity});
  return [timeSlotStart, timeSlotEnd];
};

export const blockToPingData = (pings: IPing[], timeSlotNo: number, granularity: number): IUpTimeBlock => {
  const [timeSlotStart, timeSlotEnd] = getTimeSlotDates(timeSlotNo, granularity);
  const filteredPings = pings.filter(ping => moment(ping.date).isBetween(timeSlotStart, timeSlotEnd));
  return {
    successPings: filteredPings.filter(ping => ping.success).length,
    totalPings: filteredPings.length,
    from: timeSlotStart.toDate(),
    to: timeSlotEnd.toDate(),
    pings: filteredPings
  };
};
