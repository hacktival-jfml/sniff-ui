import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent} from './dashboard/dashboard.component';
import { SingleDeviceComponent} from './single-device/single-device.component';
import { SettingsComponent} from './settings/settings.component';
import {SingleEndpointComponent} from './single-endpoint/single-endpoint.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'single-device/:ip', component: SingleDeviceComponent},
  {path: 'single-endpoint/:endpointid', component: SingleEndpointComponent},
  {path: 'settings', component: SettingsComponent},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
