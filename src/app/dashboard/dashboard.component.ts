import { Component, OnInit } from '@angular/core';
import {IDeviceData, IDeviceDataWithPings, IEndpointData} from '../types';
import {ApiService} from '../api/api.service';
import {IntervalStorageService} from '../interval-storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  deviceData: IDeviceData[];
  endpointData: IEndpointData[];
  interval: number;

  constructor(private apiService: ApiService, private intervalStorage: IntervalStorageService) { }

  async ngOnInit() {
    this.deviceData = await this.apiService.getAllDevices();
    this.endpointData = await this.apiService.getAllEndpoints();
    this.interval = this.intervalStorage.interval;
  }

}
