import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleEndpointComponent } from './single-endpoint.component';

describe('SingleEndpointComponent', () => {
  let component: SingleEndpointComponent;
  let fixture: ComponentFixture<SingleEndpointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleEndpointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleEndpointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
