import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../api/api.service';
import {IEndpointWithPings} from '../types';
import {renderTime} from '../utils';
import {IntervalStorageService} from '../interval-storage.service';

@Component({
  selector: 'app-single-endpoint',
  templateUrl: './single-endpoint.component.html',
  styleUrls: ['./single-endpoint.component.sass']
})
export class SingleEndpointComponent implements OnInit {

  private sub: any;
  public endpointId: number;
  public endpointData: IEndpointWithPings;
  public appEndpointOverviewInput: IEndpointWithPings;
  public renderTime = renderTime;
  public granularity: number;

  constructor(private route: ActivatedRoute, private apiService: ApiService, private intervalStorage: IntervalStorageService) { }

  ngOnInit() {
    this.granularity = this.intervalStorage.interval;

    this.sub = this.route.params.subscribe(params => {
      (async () => {
        this.endpointId = params.endpointid;
        console.log(params)
        this.endpointData = await this.apiService.getEndpoint(this.endpointId);
        this.appEndpointOverviewInput = {...this.endpointData, name: 'Earliest status'};
      })();
    });
  }

}
