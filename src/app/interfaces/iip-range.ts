export interface IIPRange {
  id: number;
  ip: string;
}
