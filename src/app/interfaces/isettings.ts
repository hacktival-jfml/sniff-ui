import {Endpoints, IpAddress} from './ip-address';
import {IIPRange} from './iip-range';
import {IDeviceData} from '../types';

export interface ISettings {
  time: number;
  localRange: IIPRange;
  blackListIP: IpAddress[];
  externalIP: IpAddress[];
  endpoints: Endpoints[];
}
