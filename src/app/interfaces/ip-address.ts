import {IpAddressType} from '../enum/ip-address-type.enum';

export interface IpAddress {
  id: number;
  name: string;
  ip: string;
}

export interface Endpoints extends  IpAddress {
  method: string;
}
