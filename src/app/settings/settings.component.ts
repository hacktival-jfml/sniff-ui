import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api/api.service';
import {ISettings} from '../interfaces/isettings';
import {Endpoints, IpAddress} from '../interfaces/ip-address';
import {IntervalStorageService} from '../interval-storage.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass']
})
export class SettingsComponent implements OnInit {
  public settings: ISettings;
  public dirtyDeviceIP = false;
  public dirtyEndpoint = false;
  public dirtyBlacklistedIP = false;
  public newDeviceIP: string;
  public newDeviceName: string;
  public newBlacklistedIP: string;
  public newEndpointURL: string;
  public newEndpointName: string;
  public newEndpiontMethod: string;
  public ipToScan: string;

  constructor(private apiService: ApiService, private intervalStorage: IntervalStorageService) {
  }

  ngOnInit() {
    this.setSettings();
  }

  public scan(): void {
    this.apiService.discoverDevices(this.ipToScan).then(x => {
      this.apiService.getDevices().then(y => {
        this.settings.externalIP = y;
      });
    });
  }

  public saveExternalIP(event: IpAddress) {
    const index = this.settings.externalIP.findIndex(ips => ips.id === event.id);
    if (index > -1) {
      this.settings.externalIP[index] = event;
      // this.apiService.setSettings(this.settings);
      this.apiService.updateDevice(event.id, {deviceId: event.id, ip: event.ip, name: event.name});
    }
  }

  public removeExternalIP(event: number) {
    const index = this.settings.externalIP.findIndex(ips => ips.id === event);
    if (index > -1) {
      this.apiService.deleteDevice(event);
      this.settings.externalIP.splice(index, 1);
    }
  }

  public removeEndpoint(event: number) {
    const index = this.settings.endpoints.findIndex(ips => ips.id === event);
    if (index > -1) {
      this.apiService.deleteEndpoint(event);
      this.settings.endpoints.splice(index, 1);
    }
  }

  public removeBlacklistedIP(event: number) {
    const index = this.settings.blackListIP.findIndex(ips => ips.id === event);
    if (index > -1) {
      this.settings.blackListIP.splice(index, 1);
    }
    console.log(event);
  }

  public saveEndpoint(event: Endpoints) {
    console.log(event);
    const index = this.settings.endpoints.findIndex(ips => ips.id === event.id);
    if (index > -1) {
      this.settings.endpoints[index] = event;
      this.apiService.updateEndpiont(event.id, {
        endpointId: event.id,
        url: event.ip,
        name: event.name,
        method: event.method
      });
    }
  }

  private async setSettings() {
    const intervalStorage = this.intervalStorage;

    this.settings = {
      blackListIP: [],
      externalIP: [],
      endpoints: [],
      localRange: {
        id: 1,
        ip: '192.',
      },
      time: intervalStorage.interval,
    };
    this.settings.blackListIP = [];
    this.settings.externalIP = await this.apiService.getDevices();
    this.settings.endpoints = await this.apiService.getEndpoints();
    this.settings.localRange.ip = '12';
    this.settings.time = intervalStorage.interval;
  }


  public saveBlacklistedIP(event: IpAddress) {
    const index = this.settings.blackListIP.findIndex(ips => ips.id === event.id);
    if (index > -1) {
      this.settings.blackListIP[index] = event;
      this.apiService.setSettings(this.settings);
    }
    console.log(event);
  }

  public saveIntervall() {
    // this.apiService.setSettings(this.settings);
    this.intervalStorage.interval = this.settings.time;
    console.log(this.settings.time);
  }

  public saveLocalRange() {

  }

  public makeDeviceDirty() {
    this.dirtyDeviceIP = !this.dirtyDeviceIP;
  }

  public makeBlacklistedIPDirty() {
    this.dirtyBlacklistedIP = !this.dirtyBlacklistedIP;
  }

  public makeEndpointDirty() {
    this.dirtyEndpoint = !this.dirtyEndpoint;
  }

  public async addDevice() {
    const index = this.settings.externalIP.length;
    this.settings.externalIP.push({
      id: null,
      ip: this.newDeviceIP,
      name: this.newDeviceName
    });
    this.settings.externalIP[index].id = await this.apiService.addDevice(this.newDeviceName, this.newDeviceIP);
    this.newDeviceName = null;
    this.newDeviceIP = null;
    this.makeDeviceDirty();
  }

  public async addEndpoint() {
    const index = this.settings.endpoints.length;
    this.settings.endpoints.push({
      id: null,
      ip: this.newEndpointURL,
      name: this.newEndpointName,
      method: this.newEndpiontMethod
    });
    this.settings.endpoints[index].id = await this.apiService.addEndpoint(this.newEndpointName, this.newEndpointURL, this.newEndpiontMethod);
    this.newEndpointName = null;
    this.newEndpointURL = null;
    this.newEndpiontMethod = null;
    this.makeEndpointDirty();
  }

  public addBlacklistedIP() {
    const index = this.settings.blackListIP.length - 1;
    this.settings.blackListIP.push({
      id: index + 3,
      ip: this.newBlacklistedIP,
      name: 's'
    });
    this.newBlacklistedIP = null;
    this.makeBlacklistedIPDirty();
  }


}
