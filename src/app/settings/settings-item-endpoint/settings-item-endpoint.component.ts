import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Endpoints, IpAddress} from '../../interfaces/ip-address';

@Component({
  selector: 'app-settings-item-endpoint',
  templateUrl: './settings-item-endpoint.component.html',
  styleUrls: ['./settings-item-endpoint.component.sass']
})
export class SettingsItemEndpointComponent implements OnInit {

  @Input() public endpiont: Endpoints;
  @Output() public remove = new EventEmitter<number>();
  @Output() public save = new EventEmitter<Endpoints>();
  public ipO: string;
  public name0: string;
  public methodO: string;
  constructor() { }

  ngOnInit() {
    this.ipO = this.endpiont.ip;
    this.name0 = this.endpiont.name;
    this.methodO = this.endpiont.method;
  }

  public delete() {
    this.remove.emit(this.endpiont.id);
  }

  public submit() {
    this.save.emit(this.endpiont);
    this.ngOnInit();
  }

  public discard() {
    this.endpiont.ip = this.ipO;
    this.endpiont.name = this.name0;
    this.endpiont.method = this.methodO;
  }

}
