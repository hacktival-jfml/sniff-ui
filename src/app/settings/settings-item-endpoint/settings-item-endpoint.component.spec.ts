import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsItemEndpointComponent } from './settings-item-endpoint.component';

describe('SettingsItemEndpointComponent', () => {
  let component: SettingsItemEndpointComponent;
  let fixture: ComponentFixture<SettingsItemEndpointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsItemEndpointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsItemEndpointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
