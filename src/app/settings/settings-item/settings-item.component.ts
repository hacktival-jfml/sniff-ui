import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IpAddressType} from '../../enum/ip-address-type.enum';
import {IpAddress} from '../../interfaces/ip-address';

@Component({
  selector: 'app-settings-item',
  templateUrl: './settings-item.component.html',
  styleUrls: ['./settings-item.component.sass']
})
export class SettingsItemComponent implements OnInit {

  @Input() public device: IpAddress;
  @Output() public remove = new EventEmitter<number>();
  @Output() public save = new EventEmitter<IpAddress>();
  public ipO: string;
  public name0: string;
  constructor() { }

  ngOnInit() {
    this.ipO = this.device.ip;
    this.name0 = this.device.name;
  }

  public delete() {
    this.remove.emit(this.device.id);
  }

  public submit() {
    this.save.emit(this.device);
    this.ngOnInit();
  }

  public discard() {
    this.device.ip = this.ipO;
    this.device.name = this.name0;
  }

}
