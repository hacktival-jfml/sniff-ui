import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FormsModule } from '@angular/forms';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { SingleDeviceComponent } from './single-device/single-device.component';
import { DeviceOverviewComponent } from './graphs/device-overview/device-overview.component';
import { NavbarComponent } from './navbar/navbar.component';
import {faCog, faMinus, faCheck, faTrashAlt, faWindowClose, faExclamationTriangle} from '@fortawesome/free-solid-svg-icons';
import { SettingsItemComponent } from './settings/settings-item/settings-item.component';

library.add(faCog, faMinus, faCheck, faTrashAlt, faWindowClose);
import { UptimeLinechartComponent } from './graphs/uptime-linechart/uptime-linechart.component';
import { SingleEndpointComponent } from './single-endpoint/single-endpoint.component';
import { SettingsItemEndpointComponent } from './settings/settings-item-endpoint/settings-item-endpoint.component';
import { FooterComponent } from './footer/footer.component';
import { UptimeDonutchartComponent } from './graphs/uptime-donutchart/uptime-donutchart.component';
import { UptimeAggregatedComponent } from './graphs/uptime-aggregated/uptime-aggregated.component';

library.add(faCog, faMinus, faExclamationTriangle, faCheck);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SettingsComponent,
    NavbarComponent,
    SingleDeviceComponent,
    NavbarComponent,
    SettingsItemComponent,
    DeviceOverviewComponent,
    UptimeLinechartComponent,
    SingleEndpointComponent,
    SettingsItemEndpointComponent,
    SingleEndpointComponent,
    FooterComponent,
    UptimeDonutchartComponent,
    UptimeAggregatedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
