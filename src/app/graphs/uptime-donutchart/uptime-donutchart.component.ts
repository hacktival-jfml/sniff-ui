import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {getUpTime, renderTime} from '../../utils';
import {IDeviceDataWithPings, IEndpointWithPings} from '../../types';
import Chart from 'chart.js';
import * as color from 'color';

@Component({
  selector: 'app-uptime-donutchart',
  templateUrl: './uptime-donutchart.component.html',
  styleUrls: ['./uptime-donutchart.component.sass']
})
export class UptimeDonutchartComponent implements OnInit, AfterContentInit {

  @Input('data') data: IDeviceDataWithPings | IEndpointWithPings;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    const canvas = document.getElementById('uptime-donutchart-canvas') as HTMLCanvasElement;
    const [success, nodata, total] = getUpTime(this.data);
    const colors = ['#009432', '#eee', '#e74c3c'];

    const myLineChart = new Chart(canvas, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [success, nodata, (total - success - nodata)],
          backgroundColor: colors,
          hoverBackgroundColor: colors.map(c => color(c).darken(.05).toString()),
          borderWidth: 0
        }],
        labels: ['Online', 'No data available', 'Offline'],
      },
      options: {
        maintainAspectRatio: false,
      }
    });
  }
}
