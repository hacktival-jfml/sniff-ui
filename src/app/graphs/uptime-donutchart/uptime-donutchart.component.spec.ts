import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UptimeDonutchartComponent } from './uptime-donutchart.component';

describe('UptimeDonutchartComponent', () => {
  let component: UptimeDonutchartComponent;
  let fixture: ComponentFixture<UptimeDonutchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UptimeDonutchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UptimeDonutchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
