import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {blockToPingData, getUpTime, renderDate, renderTime} from '../../utils';
import {IDeviceDataWithPings, IEndpointWithPings} from '../../types';
import Chart from 'chart.js';
import * as moment from 'moment';

@Component({
  selector: 'app-uptime-aggregated',
  templateUrl: './uptime-aggregated.component.html',
  styleUrls: ['./uptime-aggregated.component.sass']
})
export class UptimeAggregatedComponent implements OnInit, AfterContentInit {

  @Input('data') data: IDeviceDataWithPings | IEndpointWithPings;
  @Input('timeSlotSize') timeSlotSize: number;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {

    const canvas = document.getElementById('uptime-aggregated-canvas') as HTMLCanvasElement;
    const [success, nodata, total] = getUpTime(this.data);
    const colors = ['#009432', '#e74c3c'];

    const blocks = '.'
      .repeat(20)
      .split('')
      .map((_, i) => i)
      .reverse()
      .map(b => blockToPingData(this.data.status, b, this.timeSlotSize))
      .map(d => ({ success: d.successPings, failed: d.totalPings - d.successPings }));

    const datasets = ['success', 'failed'].map(d => ({
      label: d,
      backgroundColor: d === 'success' ? colors[0] : colors[1],
      data: blocks.map(b => b[d])
    }));

    const myLineChart = new Chart(canvas, {
      type: 'bar',
      data: {
        labels: blocks
          .map((d, i) => renderDate(moment(new Date()).subtract(i * this.timeSlotSize, 'minute').toDate()))
          .reverse(),
        datasets
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            stacked: true,
          }],
          yAxes: [{
            stacked: true
          }]
        }
      }
    });
  }

}
