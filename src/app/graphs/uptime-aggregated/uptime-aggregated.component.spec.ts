import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UptimeAggregatedComponent } from './uptime-aggregated.component';

describe('UptimeAggregatedComponent', () => {
  let component: UptimeAggregatedComponent;
  let fixture: ComponentFixture<UptimeAggregatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UptimeAggregatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UptimeAggregatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
