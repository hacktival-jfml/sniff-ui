import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {
  IDeviceData,

  IEndpointData,
  IEndpointPing,

  IPing, IUpTimeBlock,
  IUpTimeBlocksInformation
} from '../../types';
import * as color from 'color';
import {blockToPingData, renderTime} from '../../utils';
import {ApiService} from '../../api/api.service';


@Component({
  selector: 'app-device-overview',
  templateUrl: './device-overview.component.html',
  styleUrls: ['./device-overview.component.sass']
})
export class DeviceOverviewComponent implements OnInit, OnChanges {

  @Input('devices') devices?: Array<IDeviceData & IUpTimeBlocksInformation>;
  @Input('endpointData') endpoints?: Array<IEndpointData & IUpTimeBlocksInformation>;

  @Input('granularity') granularity: number;
  @Input('amountOfTimeBlocks') amountOfTimeBlocks: number;

  public getColor = perc => color('#009432').mix(color('#e74c3c'), 1 - perc);
  public renderTime = date => renderTime(date);
  public get data() { return this.devices || this.endpoints; }

  public getName(data: IDeviceData | IEndpointData) {
    return data.name || '';
  }

  public getIdentifier(data: IDeviceData | IEndpointData) {
    return '(' + ((data as IDeviceData).ip || (data as IEndpointData).url) + ')';
  }

  public getPingDescription(ping: IPing | IEndpointPing) {
    const devicePing = ping as IPing;
    const endpointPing = ping as IEndpointPing;

    if (endpointPing.statusCode) {
      return `${this.renderTime(endpointPing.date)} (Code ${endpointPing.statusCode}): ${endpointPing.statusMessage}`;
    } else {
      return `${this.renderTime(devicePing.date)}`;
    }
  }

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {}

  ngOnChanges() {
    const upTimeBlocks = '.'.repeat(this.amountOfTimeBlocks).split('').map((_, i) => i).reverse();

    (async () => {
      if (this.devices) {
        for (const device of this.devices) {
          const completeData = await this.apiService.getDevice(device.deviceId);
          this.devices = this.devices.map(d => d.deviceId === device.deviceId ? completeData : d);
        }

        this.devices = this.devices.map(d => ({
          ...d,
          upTimeBlocks: upTimeBlocks.map(b => blockToPingData(d.status, b, this.granularity))
        }));
      } else if (this.endpoints) {
        for (const endpoint of this.endpoints) {
          const completeData = await this.apiService.getEndpoint(endpoint.endpointId);
          this.endpoints = this.endpoints.map(e => e.endpointId === endpoint.endpointId ? completeData : e);
        }

        this.endpoints = this.endpoints.map(d => ({
          ...d,
          upTimeBlocks: upTimeBlocks.map(b => blockToPingData(d.status, b, this.granularity))
        }));
      }
    })();
  }

}
