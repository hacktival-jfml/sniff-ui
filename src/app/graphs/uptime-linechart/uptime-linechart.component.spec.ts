import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UptimeLinechartComponent } from './uptime-linechart.component';

describe('UptimeLinechartComponent', () => {
  let component: UptimeLinechartComponent;
  let fixture: ComponentFixture<UptimeLinechartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UptimeLinechartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UptimeLinechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
