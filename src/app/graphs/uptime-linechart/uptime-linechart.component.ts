import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import Chart from 'chart.js';

@Component({
  selector: 'app-uptime-linechart',
  templateUrl: './uptime-linechart.component.html',
  styleUrls: ['./uptime-linechart.component.sass']
})
export class UptimeLinechartComponent implements OnInit, AfterContentInit {

  @Input('deviceData') deviceData: IDeviceDataWithPings;

  constructor() { }

  ngOnInit() {}

  ngAfterContentInit() {
    const canvas = document.getElementById('uptime-linechart-canvas') as HTMLCanvasElement;
    const pings = this.deviceData.status;

    const myLineChart = new Chart(canvas, {
      type: 'line',
      data: {
        labels: pings.map(ping => renderTime(ping.date)).reverse().slice(0, 100).reverse(),
        datasets: [{
          data: pings.map(ping => ping.success ? 1 : 0).reverse().slice(0, 100).reverse(),
          label: 'Pings',
          borderColor: '#009432',
          fill: false
        }]
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              stepSize: 1
            }
          }]
        }
      }
    });
  }

}

import {IDeviceDataWithPings} from '../../types';
import {renderTime} from '../../utils';
