import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  public versionData: string;

  constructor(private apiService: ApiService, private modalService: NgbModal) { }

  public open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  ngOnInit() {
    (async () => {
      this.versionData = await this.apiService.getVersionData();
    })();
  }

}
