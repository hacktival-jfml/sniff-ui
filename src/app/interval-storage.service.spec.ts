import { TestBed } from '@angular/core/testing';

import { IntervalStorageService } from './interval-storage.service';

describe('IntervalStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IntervalStorageService = TestBed.get(IntervalStorageService);
    expect(service).toBeTruthy();
  });
});
