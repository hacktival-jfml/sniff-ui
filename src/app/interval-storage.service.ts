import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IntervalStorageService {
  public interval = 10;

  constructor() { }
}
