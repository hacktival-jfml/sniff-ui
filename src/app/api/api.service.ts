import {Injectable} from '@angular/core';
import {ISettings} from '../interfaces/isettings';
import {IDeviceData, IDeviceDataWithPings, IEndpointData, IEndpointWithPings} from '../types';
import {Endpoints, IpAddress} from '../interfaces/ip-address';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly url: string;

  constructor() {
    this.url = environment.apiUrl;
  }

  public setSettings(settings: ISettings) {
  }

  public async updateDevice(deviceId: number, device: IDeviceData): Promise<boolean> {
    const res = await fetch(`${this.url}/device/update/${deviceId}`, {
      method: 'POST',
      body: JSON.stringify(device),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    return res.ok;
  }

  public async updateEndpiont(endpointId: number, endpoint: IEndpointData): Promise<boolean> {
    const res = await fetch(`${this.url}/endpoint/update/${endpointId}`, {
      method: 'POST',
      body: JSON.stringify(endpoint),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return res.ok;
  }

  public async addDevice(alias: string, ipt: string): Promise<any> {
    const device = {
      name: alias,
      ip: ipt
    };
    const pu = await fetch(`${this.url}/device`, {
      method: 'PUT',
      body: JSON.stringify(device),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const json = await pu.json();
    return json.deviceId;
  }


  public async deleteDevice(id: number) {
    const pu = fetch(`${this.url}/device/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  public async discoverDevices(ip: string): Promise<Array<IDeviceData>> {
    return await this.fetch(`${this.url}/device/auto-discover/${ip}/`);
  }

  public async getDevices(): Promise<IpAddress[]> {
    const req = await fetch(`${this.url}/device`);
    let json = await req.json();
    json = json.map(address => ({
      ...address,
      name: address.name,
      id: address.deviceId
    } as IpAddress));
    return json;
  }

  public async getEndpoints(): Promise<Endpoints[]> {
    const req = await fetch(`${this.url}/endpoint`);
    let json = await req.json();
    json = json.map(address => ({
      ...address,
      name: address.name,
      id: address.endpointId,
      methods: address.method,
      ip: address.url
    } as IpAddress));
    return json;
  }

  public async addEndpoint(alias: string, ipt: string, methodt: string): Promise<any> {
    const endpoint = {
      name: alias,
      url: ipt,
      method: methodt
    };
    const pu = await fetch(`${this.url}/endpoint`, {
      method: 'PUT',
      body: JSON.stringify(endpoint),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const json = await pu.json();
    return json.endpointId;
  }

  public async deleteEndpoint(id: number) {
    const pu = fetch(`${this.url}/endpoint/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  public async getVersionData(): Promise<string> {
    const res = await fetch(`${this.url}/version`);
    const text = await res.text();
    return text.split('\n').slice(0, 2).join(' - ');
  }

  public async getAllDevices(): Promise<Array<IDeviceData>> {
    return await this.fetch(`${this.url}/device`);
  }

  public async getDevice(deviceId: number): Promise<IDeviceDataWithPings> {
    const json = await this.fetch(`${this.url}/device/${deviceId}`);
    json.status = (json as IDeviceDataWithPings).status.map(ping => ({
      ...ping,
      date: new Date(ping.date),
      success: (ping as any).status === 'online'
    }));
    return json;
  }

  public async getDeviceByIp(ip: string): Promise<IDeviceDataWithPings> {
    const devices = await this.getAllDevices();
    const deviceId = devices.find(d => d.ip === ip).deviceId;
    return await this.getDevice(deviceId);
  }

  public async getAllEndpoints(): Promise<Array<IEndpointData>> {
    return await this.fetch(`${this.url}/endpoint`);
  }

  public async getEndpoint(endpointId: number): Promise<IEndpointWithPings> {
    const json: IEndpointWithPings = await this.fetch(`${this.url}/endpoint/${endpointId}`);

    json.status = json.status.map(s => ({
      ...s,
      date: new Date(s.date),
      success: s.statusCode === 200
    }));

    return json;
  }

  private async fetch(url: string) {
    const res = await fetch(url);
    const json = await res.json();
    return json;
  }
}
