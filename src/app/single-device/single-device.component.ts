import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../api/api.service';
import {IDeviceData, IDeviceDataWithPings} from '../types';
import {getUpTime} from '../utils';
import {IntervalStorageService} from '../interval-storage.service';

@Component({
  selector: 'app-single-device',
  templateUrl: './single-device.component.html',
  styleUrls: ['./single-device.component.sass']
})
export class SingleDeviceComponent implements OnInit, OnDestroy {

  private sub: any;
  public ip: string;
  public deviceData: IDeviceData;
  public appDeviceOverviewInput: IDeviceData;
  public upTime: string;
  public granularity: number;

  constructor(private route: ActivatedRoute, private apiService: ApiService, private intervalStorage: IntervalStorageService) {}

  ngOnInit() {
    this.granularity = this.intervalStorage.interval;

    this.sub = this.route.params.subscribe(params => {
      (async () => {
        this.ip = params.ip;
        this.deviceData = await this.apiService.getDeviceByIp(this.ip);
        this.appDeviceOverviewInput = {...this.deviceData, ip: 'Earliest status', name: undefined};

        const [success, nodata, total] = getUpTime(this.deviceData as IDeviceDataWithPings);
        this.upTime = `${Math.floor(1000 * success / total) / 10}% of the time online, ${Math.floor(1000 * nodata / total) / 10}% of`
          + ` the time were no data recorded.`;
      })();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
