FROM nginx:alpine

COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY conf/mime.types /etc/nginx/conf/mime.types

WORKDIR /usr/share/nginx/html
COPY dist/sniff-ui/ /var/www

ENV API_URL="localhost:8080/api"

CMD sed -i "s|replaced_by_script|$API_URL|g" /var/www/*.js && \
  nginx -g 'daemon off;'
